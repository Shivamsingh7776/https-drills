from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import uuid
import time
import requests


HOST = "localhost"
PORT = 8001  


class showing_html(BaseHTTPRequestHandler):

    def do_GET(self):
        if (self.path.endswith("/html")):
            try:
                file_to_open = open('home.html').read()
                self.send_response(200)
            except FileNotFoundError:
                file_to_open = "File Not Found"
                self.send_response(404)

            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes(file_to_open, 'utf-8'))
            # self.wfile.write(file_to_open.encode())
        elif (self.path.endswith("/json")):
            try:
                file_to_open = open('index.json').read()
                self.send_response(200)
            except FileNotFoundError:
                file_to_open = "File Not found"
                self.send_response(404)
            self.send_header("Content-type", "text/json")
            self.end_headers()
            self.wfile.write(bytes(file_to_open, 'utf-8'))

        elif (self.path.endswith("/uuid")):
            uid = str({"uuid": uuid.uuid4()})
            uid = json.dumps(uid)
            self.send_response(200)
            self.send_header("Content-type", "text/json")
            self.end_headers()
            self.wfile.write(bytes(uid, 'utf-8'))
        elif(self.path[-11:-3] == "/status/"):
            status = self.path[-3:]
            self.send_response(int(status))
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes(status.encode()))
        elif(self.path[0:7] == "/delay/"):
            delay = self.path[7:]
            time.sleep(int(delay))
            self.send_response(int(delay))
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes(delay.encode()))
def main():
    Server = HTTPServer((HOST, PORT), showing_html)

    print("Server is running...")

    Server.serve_forever()
    Server.server_close()

    print("Server closed!")


if __name__ == "__main__":
    main()
